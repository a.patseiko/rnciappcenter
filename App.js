import React, { useState, Component } from "react";
import moment from "moment";
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  TouchableWithoutFeedback,
  ActivityIndicator
} from "react-native";

const list = [];

for (let i = 0; i < 50; i += 1) {
  const res = {
    title: moment()
      .add(i, "day")
      .format("DD MM YYYY"),
    data: []
  };
  for (let x = 0; x < 10; x += 1) {
    res.data.push({ title: `section ${i}, row-${x}`, id: `${i}-${x}` });
  }
  list.push(res);
}

class Item extends Component {
  shouldComponentUpdate(nextProps) {
    // console.log(
    //   "nextProps",
    //   nextProps.id,
    //   this.props.activeSlot,
    //   nextProps.id !== this.props.activeSlot
    // );
    return (
      nextProps.id === this.props.activeSlot ||
      (this.props.activeSlot === this.props.id && !nextProps.activeSlot)
    );
  }

  render() {
    const { setActiveSlot, title, id, activeSlot } = this.props;
    console.log("render");
    return (
      <TouchableWithoutFeedback
        testID={`increase-count-${id}`}
        accessibilityLabel={`increase-count-${id}`}
        onPress={() => {
          setActiveSlot(id);
        }}
      >
        <View
          style={[
            styles.item,
            activeSlot === id ? { backgroundColor: "red" } : null
          ]}
        >
          <Text style={styles.title}>{title}</Text>
          {activeSlot === id ? <Text>this row was selected</Text> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      activeSlot: null,
      loadingMore: false
    };
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return false;
  // }

  _renderFooter() {
    const { loadingMore } = this.state;
    if (!loadingMore) return null;

    return (
      <View>
        <ActivityIndicator animating size="large" />
      </View>
    );
  }

  render() {
    const { activeSlot } = this.state;
    return (
      <View testID="app-root" accessibilityLabel="app-root">
        <SectionList
          removeClippedSubviews={true}
          sections={list}
          extraData={list}
          keyExtractor={item => item.id}
          initialNumToRender={27}
          windowSize={41}
          renderItem={({ item }) => (
            <Item
              {...item}
              setActiveSlot={id => this.setState({ activeSlot: id })}
              activeSlot={activeSlot}
            />
          )}
          renderSectionHeader={({ section: { title } }) => (
            <Text style={styles.header}>{title}</Text>
          )}
          ListFooterComponent={this._renderFooter()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 16
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8
  },
  header: {
    fontSize: 32
  },
  title: {
    fontSize: 24
  }
});
